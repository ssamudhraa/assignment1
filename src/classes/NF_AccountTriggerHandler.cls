/*
* @File Name:   NF_AccountTriggerHandler.cls
* @Description:
* @Author:      samudhraa
* @Group:       Apex
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver       Date        Author      Modifications
* 1.0       2017-05-09  Recruiter    Created the file/class
*/
public with sharing class NF_AccountTriggerHandler extends NF_AbstractTriggerHandler {

    private List<Contact> contactsCreated = new List<Contact>();
        
    public override void beforeUpdate(){

    }

    public override void afterUpdate(){

    }

    public override void beforeInsert(){

    }

    public override void afterInsert(){
        createContactFromAccount(Trigger.new);
    }

    public override void afterDelete(){

    }

    public override void andFinally(){
        if(!contactsCreated.isEmpty()){
            insert contactsCreated;
        }
    }

    public override void afterBulk(){
        
    }

    /************************************************************
     * @author      samudhraa
     * @date        2018-01-05
     * @description creates contact from account with last name same as account name to a static list
     * @param       accounts
     * @return      nothing
     ***********************************************************/

    private void createContactFromAccount(List<Account> accounts){      
        for(Account eachAccount : accounts){
            Contact c = new Contact(LastName = eachAccount.name, AccountId = eachAccount.Id);
            contactsCreated.add(c);
        }
    }
}