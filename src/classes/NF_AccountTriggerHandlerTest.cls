/**
* @File Name:   NF_AccountTriggerHandlerTest.cls
* @Description:
* @Author:      samudhraa
* @Group:       Apex
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver       Date        Author      Modification
* 1.0                   samudhraa    Created the file/class
 */

@IsTest
private class NF_AccountTriggerHandlerTest {
    static Integer batchSize = 10;
    
    static testMethod void insertBatchOfAccountsVerifyContactCreatedWithSameName() {
        String accountName ='Acme';
        
        List<Account> listOfAccounts = new List<Account>();
        for(Integer i=0; i< batchSize; i++){
            Account acmeAccount = new Account(Name = accountName+i);
            listOfAccounts.add(acmeAccount);
        }


        Test.startTest();
        insert listOfAccounts;

        List<Contact> contactsCreated = [SELECT LastName, AccountId FROM Contact];
        System.assertEquals(batchSize, contactsCreated.size(), 'Error in number of Contacts created');
        for(Integer i=0; i< batchSize; i++){
            System.assertEquals(accountName+i,contactsCreated.get(i).LastName,'Contact name is not same as account name');    

        }
        
        Test.stopTest();
    }
}