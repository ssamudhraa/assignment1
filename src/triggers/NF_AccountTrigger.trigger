/**
* @File Name:	NF_AccountTrigger.trigger
* @Description: An example trigger for testing
* @Author:   	samudhraa
* @Group:   	Trigger
* @Modification Log	:
*-------------------------------------------------------------------------------------
* Ver       Date        Author      Modification
* 1.0       2017-05-09  Recruiter    Created the file/class
*/

trigger NF_AccountTrigger on Account (
	before insert, 
	before update, 
	before delete, 
	after insert, 
	after update, 
	after delete, 
	after undelete) {
    NF_TriggerFactory.CreateHandlerAndExecute(Account.SObjectType);
}